public class Calculator {
    public static int add(int x, int y) {
        return x + y;
    }

    public static int subtract(int x, int y) {
        return x - y;
    }

    public static int multiply(int x, int y) {
        return x * y;
    }

    public static void main(String[] args) {
        if (args.length == 3) {
            int first_operand = Integer.parseInt(args[0]);
            String operation = args[1];
            int second_operand = Integer.parseInt(args[2]);
            if (operation.equals("+")) {
                System.out.println(add(first_operand, second_operand));
            } else if (operation.equals("-")) {
                System.out.println(subtract(first_operand, second_operand));
            } else if (operation.equals("*")) {
                System.out.println(multiply(first_operand, second_operand));
            } else {
                System.err.println("Unsupported operation!");
                System.exit(1);
            }
        } else {
            System.err.println("Usage: java Calculator <x> <operation> <y>");
            System.exit(1);
        }
    }
}
